# CATracker Tools
Tools für CATracker ein "Full Stack" Sensor IOT Projekt

## TODO
* [ ] GPS Daten festlegen/ermitteln
    - [ ] GnssToolKit3 https://github.com/zxcwhale/GnssToolKit3-binaries/releases
    - [ ] Low power modes
    - [ ] Baud Raten Umschaltung

* [ ] STM Eval Board mit Software
    - [ ] Datenaquise
    - [ ] Modell erstellen
